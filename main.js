async function squareFunction() {
    let x, text;

    // Get the value of the input field with id="numb"
    x = document.getElementById("zapytanie").value

    // If x is Not a Number or less than one or greater than 10
    if (isNaN(x) || x < 1 || x > 650) {
        text = "Input not valid. Must be a number between 1 and 650.";
    }
    else {
       const response = await fetch( "http://localhost:3000/squares" , {
            method: "post",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "initNumber" : document.getElementById("zapytanie").value
            })
        });
        text = await response.json();
    }
    document.getElementById("wynik").innerHTML = text;
}
